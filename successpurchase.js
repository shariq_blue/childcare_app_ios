import React, { Component } from 'react';
import { Text, View,AppRegistry,TouchableOpacity,StyleSheet,Image } from 'react-native';

const imgg=require('./assets/checked.png')
export default class Successpurchase extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={imgg} style={{width:"21%",height:"13%",marginTop:"30%"}}></Image>
        <Text style={styles.headline}>
            You purchased practitioner's detail's successfully, You can check them on cart->History.  
        </Text>
        <TouchableOpacity style={styles.loginbtn} onPress={()=>this.props.navigation.navigate('BUCKETHISTORY')}>
        <Text style={{color:"#fff",fontSize:20}}>OK</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    
  },
    headline: {
        textAlign: 'center', // <-- the magic
        fontSize: 18,
        marginTop: 90,
        width: 280,
        marginBottom:30
      },
        loginbtn:{
      backgroundColor:"#fac1b8",
      width:"33%",
      alignItems: 'center',
      padding:10,
      borderRadius:6
    }
  
  });
  


AppRegistry.registerComponent('Successpurchase',()=>Successpurchase)
